require 'nokogiri'
require 'open-uri'
require 'csv'
 
# Store URL to be scraped
base_url = "https://www.airbnb.com/s/Plano-Texas"
site_url = "https://www.airbnb.com" 

# Parse the page with Nokogiri
page = Nokogiri::HTML(open(base_url))
 

page_numbers = []
page.css("div.pagination ul li a[target]").each do |line|
 page_numbers << line["target"].strip.to_i
end

max_page = page_numbers.max

# Initialize arrays
name = []
price = []
details = []
location = []
link = []

# Loop through all pages
max_page.times do |i|

	url = base_url + "?page=#{i+1}"
  page = Nokogiri::HTML(open(url))

	# Store data in arrays

	page.css('h3[itemprop="name"]').each do |line|
	  name << line.text.strip
	end
	
	page.css('span.h3.price-amount').each do |line|
	  price << line.text.strip
	end
	
	page.css('div[itemprop="description"]').each do |line|
	  details << line.text.strip.split(/ · /)
	end
	
	page.css('p.address').each do |line|
	  location << line.text.strip
	end

	page.css("a.media-photo.media-cover[href]").each do |line|
	  link << site_url + line["href"]
	end

end

# Write data to CSV file
CSV.open("airbnb-listing.csv", "w") do |file|
	file << ["Listing Name", "Price", "Room Type", "Reviews", "Location", "Link"]
	name.length.times do |i| 
		file << [name[i], price[i], details[i][0].strip, details[i][2], location[i], link[i]]
	end
end