require 'nokogiri'
require 'open-uri'
require 'csv'

# Store URL to be scraped
base_url = "https://web.archive.org/web/2013021506253N4/http://www.match.com:81/cp.aspx?cpp=/cppp/index/domestic.html&ER=sessiontimeout
"
site_url = "https://web.archive.org"

Dir.glob('pages/*.html') do |rb_file|
  # do work on files ending in .rb in the desired directory

# File.open("match_com_archive_urls.txt").readlines.each do |base_url|
	# file_name = 'pages/' + base_url.strip.match(/\d+/).to_s + '.html'

	# my_local_file = open(file_name, "w")

	# puts "Scraping #{base_url.strip}"

	# begin
		page = Nokogiri::HTML(open(rb_file))

		# redirect = page.css('p.impatient a') #try(:[],'href')

		# redirect = site_url + redirect[0]['href'].to_s unless redirect.empty?

		# puts "Redirecting to " + redirect unless redirect.empty?
		# page = Nokogiri::HTML(open(redirect.strip)) unless redirect.empty?
	# rescue OpenURI::HTTPError => e
		# page = "Exception: #{e}. #{base_url.strip}"
	# end

	# my_local_file.write(page)
	# puts page
	# my_local_file.close
	# sleep 5

	urls = {}
	page.css('a').each do |line|
	  url = site_url + line['href'] if line['href'].to_s.include?('/web/2') and line['href'].to_s.include?('qsearch/qsearch')
	  url = url.to_s.strip
		urls[url] = url unless url.empty?
	end

	urls.each do |ind, url|
		puts url
	end
end