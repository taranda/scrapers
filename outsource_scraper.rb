require 'rubygems'
require 'mechanize'
require 'csv'


BASE_ADDRESS = 'https://www.outsource.com'
LOGIN_ADDRESS = '/login'

scraper = Mechanize.new

# Mechanize setup to rate limit your scraping to once every half-second
scraper.history_added = Proc.new { sleep 0.5 }

# Login
scraper.get(BASE_ADDRESS) do |page|
  # Click the login link
  login_page = scraper.click(page.link_with(:text => /Login/))

  
  # Submit the login form
  result = login_page.form_with(:action => BASE_ADDRESS + LOGIN_ADDRESS) do |f|
    username_field = f.field_with(:name => 'username')
    username_field.value = ARGV[0]
    password_field = f.field_with(:name => 'Password')
    password_field.value = ARGV[1]
    redirect_field = f.field_with(:name => 'redirect')
    redirect_field.value = ""
    login_field = f.field_with(:name => 'login_process')
    login_field.value = "1"
    md5pass_field = f.field_with(:name => 'md5pass')
    md5pass_utf_field = f.field_with(:name => 'md5pas_utf')
    button = f.button_with(:type => 'submit')
  end.submit
  
  # initialize data storage arrays
  link = []
  title  = []
  client = []
  budget = []
  duration = []
  posted = []
  quotes = []
  description = []

  # Calculate max page numbers
  page_numbers = []
  result.css("ul.pagination.inline-block.vertical-middle li a").each do |line|
   page_numbers << line.text.strip.to_i
  end

  max_page = page_numbers.max

  # Loop through all pages
  max_page.times do |i|

    url = BASE_ADDRESS + "/job-requests?page=#{i+1}"
    result = scraper.get(url)

    # Scrape titles and links
    result.css('div.project-list ul li a.font-18.font-semi-bold.text-normal.text-blue').each do |line|
      title << line.text.strip
      link << line['href']
    end
    # Scrape client names
    result.css('div.project-list ul li div.m-top-xs.font-16').each do |line|
      client << line.text.split(/: /)[1].strip
    end

    # Scrape budgets, durations, posted time and quotes
    result.css('div.project-list ul li div.m-top-xs div.inline-block').each do |line|
      if line.text.include? "Budget"
        budget << line.text.split(/: /)[1].delete!("|").strip
      elsif line.text.include? "Duration"
        duration << line.text.split(/: /)[1].delete!("|").strip
      elsif line.text.include? "Posted"
        posted << line.text.split(/: /)[1].delete!("|").strip
      elsif line.text.include? "Quote"
        quotes << line.text.split(/: /)[1].strip
      end
    end

    # Scrape description
    result.css('div.project-list ul li p.font-16.m-top-20').each do |line|
      description << line.text.strip
    end
  end

  # Write data to CSV file
  CSV.open("outsource.csv", "w") do |file|
    file << ["Title", "Client", "Budget", "Duration", "Posted", "Quotes", "Description", "Link"]
    title.length.times do |i| 
      file << [title[i], client[i], budget[i], duration[i], posted[i], quotes[i], description[i], link[i]]
    end
  end
end   